<!ENTITY gst-branch-stable "1.22">
<!ENTITY gst-version-stable "1.22.9">
<!ENTITY gst-version-devel "git main">

<!ENTITY orc-version-stable "0.4.36">
<!ENTITY orc-version-devel "git main">

<!ENTITY gst-bug-report "http://bugzilla.gnome.org/enter_bug.cgi?product=GStreamer">
<!ENTITY gst-repo-http "https://gitlab.freedesktop.org/gstreamer/">
<!ENTITY realsite "https://gstreamer.freedesktop.org">
<!ENTITY webarchive "https://web.archive.org/web/https://gstreamer.freedesktop.org">
<!ENTITY nbsp "&#x00A0;">
